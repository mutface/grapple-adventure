﻿using UnityEngine;
using System.Collections;

public class RepawnPointManager : MonoBehaviour {

	public bool hasBeenActivated;

	public Sprite activatedSprite;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other) {
		// Make sure it's the player and it hasnt already been activated
		if (other.CompareTag("Player") && !hasBeenActivated) {
			Activate(other.GetComponent<MyCharacterController>());
		}

	}

	void Activate(MyCharacterController PlayerController) {
		hasBeenActivated = true;
		GetComponent<SpriteRenderer>().sprite = activatedSprite;
		PlayerController.respawnPoint = transform.position;
	}
}

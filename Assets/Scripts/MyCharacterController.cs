﻿using UnityEngine;
using System.Collections;

public class MyCharacterController : MonoBehaviour {

	[Header ("Moving")]
	public float moveSpeed = 10.0f;
	public bool isFlyingThroughAir;
	[HideInInspector]
	public bool shouldFaceRight;

	[Header ("Jumping")]
	public float minJumpHeight = 4.0f;
	private float lastJumpTime;
	public float minTimeToJumpApex = 0.4f;
	public float maxTimeToJumpApex = 0.6f;
	public bool isGrounded;
	public Transform groundCheck;
	private float jumpHeldTime;
	private float jumpVelocity;
	public float gravity;
	
	private string horizontalControl = "Horizontal";
	private string jumpControl = "Jump";
	private Rigidbody2D rigidBody;

	private Animator anim;

	public float points;
	public Vector3 respawnPoint;

	// Use this for initialization
	void Start () {
		// Get the rigid body for future use
		rigidBody = GetComponent<Rigidbody2D>();
		// This script takes care of gravity
		rigidBody.gravityScale = 0.0f;

		// Get animator for use later
		anim = GetComponent<Animator>();

		// Start the player off looking right
		shouldFaceRight = true;

		// Give them a starting respawn point
		respawnPoint = transform.position;
	}
	
	// Update is called once per frame
	void Update () {

		/////////////////////////////////////
		/// DebugControls

		// Reload level
		if (Input.GetKeyDown(KeyCode.L)) Application.LoadLevel(Application.loadedLevel);

		/////////////////////////////////////

		Vector2 newVelocity = rigidBody.velocity;

		// If no grapple, give immediate response for controls
		if (!gameObject.GetComponent<SpringJoint2D>()) {

			// Player flying through air after grapple controls
			if (isFlyingThroughAir) {
				if (Mathf.Abs(Input.GetAxis(horizontalControl)) > 0.5f) {
					newVelocity.x = Input.GetAxis(horizontalControl) * moveSpeed;
				}
			}
			// Player walking on ground controls
			else newVelocity.x = Input.GetAxis(horizontalControl) * moveSpeed;
		}
		// Allow the player to move while grappled
		else {
//			newVelocity.x += Input.GetAxis(horizontalControl) * moveSpeed * Time.deltaTime;

		}

		// Figure out jump stuff
		float fullGravity = -(2.0f*minJumpHeight) / Mathf.Pow(minTimeToJumpApex, 2.0f);
		if (!isGrounded) gravity = fullGravity;

		jumpVelocity = Mathf.Abs(gravity) * minTimeToJumpApex;

		// Do this one time jump button is hit
		if (Input.GetButtonDown(jumpControl) && isGrounded) {
			lastJumpTime = Time.time;
			newVelocity.y = jumpVelocity;
		} else if (Input.GetButton(jumpControl)) {
			jumpHeldTime += Time.deltaTime;
//			float timeAtApex = lastJumpTime + (maxTimeToJumpApex-minTimeToJumpApex);
//
//			// Check if still in first half of jump
//			if (Time.time < timeAtApex) {
//				gravity*=/10.0f;
//			}
		} else {
			jumpHeldTime = 0.0f;
		}



		// Only need to add gravity if player isn't already on the ground

		if (!isGrounded) {
			if (jumpHeldTime==0.0f) gravity = fullGravity;
			else {
				float airTime = Time.time-lastJumpTime;

				float percentOfWayToApex = Mathf.Clamp01((1.0f-((maxTimeToJumpApex-airTime)/maxTimeToJumpApex)));

				gravity = fullGravity * percentOfWayToApex;
			}

			newVelocity.y += gravity * Time.deltaTime;

		}

		// Figure out what direction player should face
		if(Input.mousePosition.x > Camera.main.WorldToScreenPoint(transform.position).x) {
			shouldFaceRight = true;
		} else {
			shouldFaceRight = false;
		}

		// Flip player right direction
		if (shouldFaceRight) {
			Vector3 newScale = transform.localScale;
			newScale.x = Mathf.Abs(newScale.x);
			transform.localScale = newScale;
		} else {
			Vector3 newScale = transform.localScale;
			newScale.x = -Mathf.Abs(newScale.x);
			transform.localScale = newScale;
		}

		rigidBody.velocity = newVelocity;

		// Leave a trail of where the player has been for debuging
//		Debug.DrawRay(transform.position, -transform.up, Color.green, 5.0f);

		// Pass data to animator
		anim.SetFloat("xSpeed", Mathf.Abs(rigidBody.velocity.x));
		anim.SetFloat("ySpeed", rigidBody.velocity.y);
		anim.SetBool("goingRight", shouldFaceRight);
		anim.SetBool("onGround", isGrounded);
	}

	void FixedUpdate () {
		// Check if we're grounded
		RaycastHit2D hit = Physics2D.Linecast (transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
		if (hit) {
			isGrounded = true;
			isFlyingThroughAir = false;
		} else {
			isGrounded = false;
		}

	}

	void OnTriggerEnter2D (Collider2D other) {
		// Make sure it is a collectible
		if (other.gameObject.CompareTag("Collect")) {
			// Add points
			points += other.gameObject.GetComponent<Collectible>().pointValue;
			// Get rid of it
			Destroy(other.gameObject);
		}
	}

	public void Respawn() {
		transform.position = respawnPoint;
		rigidBody.velocity = Vector2.zero;
	}


}

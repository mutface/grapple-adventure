﻿using UnityEngine;
using System.Collections;

public class Grapple : MonoBehaviour {
	public string grappleButton = "Fire1";
	public string reelButton = "Jump";

	private SpringJoint2D spring;
	public float maxShotDistance = 10.0f;
	public float reelSpeed = 1.7f;
	public float minLength = 1.0f;
	private bool isGrappleOut;
	private bool canReel = true;

	public Material grappleMat;
	public Color grappleColor;
	public float grappleWidth = 0.5f;
	private LineRenderer grappleLine;

	public ArrayList grappleLineLocalPoints;
	public ArrayList grappleLinePointRigids;

	public bool simulateMouseDown;
	public float dampenForce = 7000.0f;
	// Use this for initialization
	void Start () {
		grappleLineLocalPoints = new ArrayList();
		grappleLinePointRigids = new ArrayList();


		grappleLine = this.gameObject.AddComponent <LineRenderer>();
		grappleLine.SetWidth (grappleWidth, grappleWidth);
		grappleLine.material = grappleMat;
//		grappleLine.material.color = grappleColor;
		grappleLine.SetColors(grappleColor,grappleColor);
		//we need to see the line... 
//		grappleLine.renderer.enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
	
		if (isGrappleOut && spring.distance > minLength && canReel) {
			// Reel the grapple in
			spring.distance -= reelSpeed * Time.deltaTime;
		}

		
		// Reel the player in on mouse held down
		if (Input.GetButton(grappleButton) || simulateMouseDown) {
			if (isGrappleOut) {
				// Reel the grapple in
//				spring.distance -= reelSpeed * Time.deltaTime;

				// Wrap around objects
				WrapAround(grappleLineLocalPoints.Count-1, false);

				// Dampen the swinging
				Rigidbody2D rig = GetComponent<Rigidbody2D>();
				rig.AddForce(-rig.velocity*Time.deltaTime*dampenForce);

			} else {
				Vector3 direction = (Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position).normalized;
				
				RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, maxShotDistance, 1 << LayerMask.NameToLayer("Ground"));
				
				if (hit && hit.collider.CompareTag("GrapplePoint")) {
					Debug.DrawRay(transform.position, direction*hit.distance,Color.red);
					grappleLineLocalPoints.Add((Vector2)hit.transform.InverseTransformPoint(hit.point));
					grappleLinePointRigids.Add(hit.rigidbody);

					AttachGrappleTo(hit.rigidbody,hit.point,hit.distance);
				}
			}
		}
		
		// Get rid of grapple point on mouse up
		if (Input.GetButtonUp(grappleButton)) {
			if (isGrappleOut) {
				Destroy(spring);
				isGrappleOut = false;
				gameObject.GetComponent<MyCharacterController>().isFlyingThroughAir=true;
				grappleLineLocalPoints.Clear();
				grappleLinePointRigids.Clear();
			}
		}

		DrawGrapple();


	}

	void WrapAround(int lastIndex, bool lastIndexHit) {
		Rigidbody2D lastGrapplePointRigid = grappleLinePointRigids[lastIndex] as Rigidbody2D;
		Vector2 lastGrapplePoint = lastGrapplePointRigid.transform.TransformPoint((Vector2)grappleLineLocalPoints[lastIndex]);
		
		RaycastHit2D hit = Physics2D.Linecast(transform.position, lastGrapplePoint, 1 << LayerMask.NameToLayer("Ground"));

		if (hit) {
			// Old place was hit
			if (hit.point==lastGrapplePoint) {
				// Make sure this isnt the last index available
				if (lastIndex != grappleLineLocalPoints.Count-1) {
					// Remove the index above this one
					grappleLineLocalPoints.RemoveAt(lastIndex+1);
					grappleLinePointRigids.RemoveAt(lastIndex+1);
					// Reattach grapple to last point
					AttachGrappleTo(hit.rigidbody,hit.point,hit.distance);

				}

				// Make sure there is another index to check
				if (grappleLineLocalPoints.Count > 1) {
					WrapAround(lastIndex-1, true);
				}
			}
			// New place was hit
			else {
				// If the last test worked, dont add anything new
				if (lastIndexHit) return;

				// Get all the verticies to test against
				Vector2[] verts = hit.collider.gameObject.GetComponent<SpriteRenderer>().sprite.vertices;
				// Setup vector2 to store closest point
				Vector2 closest = Vector2.zero;
				// Make closestDist max so any vertex will be closer
				float closestDist = float.MaxValue;

				Rigidbody2D closestRigid = null;

				// Run through all the vertexes
				for(int n=0; n < verts.Length; n++) {
					Vector2 worldPoint = hit.collider.transform.TransformPoint(verts[n]);
					float distanceToHit = Vector2.Distance(hit.point, worldPoint);
					// Check if this vertex is closer
					if (distanceToHit < closestDist) {
						// Save it to test against if it is
						closest = worldPoint;
						closestDist = distanceToHit;
						closestRigid = hit.rigidbody;
					}
				}

				// Make sure not to have dupicate points
				if (!grappleLineLocalPoints.Contains(closest)) {
					grappleLineLocalPoints.Add((Vector2)closestRigid.transform.InverseTransformPoint(closest));
					grappleLinePointRigids.Add(hit.rigidbody);
					AttachGrappleTo(hit.rigidbody,closest,Vector2.Distance(transform.position, closest));
				}
			}
			
		}

	}
	
	void AttachGrappleTo(Rigidbody2D rigid, Vector2 worldPoint, float dist) {
		spring = gameObject.GetComponent<SpringJoint2D>();
		if (!spring) spring = gameObject.AddComponent<SpringJoint2D>();
		spring.enableCollision = true;
		spring.connectedBody = rigid;
		// Connected anchor need's hit point in local coordinates on the hit object
		spring.connectedAnchor = rigid.transform.InverseTransformPoint(worldPoint);
		spring.distance = dist;
		spring.dampingRatio = 1.0f;
		spring.frequency = 8.0f;
		
		isGrappleOut = true;
	}

	void DrawGrapple() {
		if (isGrappleOut) {
			grappleLine.enabled = true;
			grappleLine.SetVertexCount (grappleLineLocalPoints.Count+1);

			grappleLine.SetPosition(0, transform.position);
			for (int n=0; n < grappleLineLocalPoints.Count; n++) {
				int pos = grappleLineLocalPoints.Count - n;
				Rigidbody2D currentRigid = grappleLinePointRigids[n] as Rigidbody2D;
				Vector2 currentPoint = (Vector2)grappleLineLocalPoints[n];
				grappleLine.SetPosition(pos, currentRigid.transform.TransformPoint(currentPoint));

			}
			grappleLine.sortingOrder=-1;

		} else {
			grappleLine.enabled = false;
		}


	}


}

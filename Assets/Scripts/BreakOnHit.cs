﻿using UnityEngine;
using System.Collections;

public class BreakOnHit : MonoBehaviour {

	public GameObject brokenPrefab;
	public float randomForceOnBreak = 5.0f;
	public float directionalForceOnBreak = 2.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.gameObject.CompareTag("Player")) {
			GameObject brokenVersion = Instantiate (brokenPrefab, transform.position, transform.rotation) as GameObject;

			foreach (Rigidbody2D pieceRigid in brokenVersion.GetComponentsInChildren<Rigidbody2D>()) {

				pieceRigid.transform.parent=null;

				Vector2 dirAwayFromPlayer = (pieceRigid.transform.position - other.transform.position).normalized;
				Vector2 randomDir = Random.insideUnitCircle.normalized;

				Vector2 directionalForce = dirAwayFromPlayer * directionalForceOnBreak * other.attachedRigidbody.velocity.magnitude;
				Debug.DrawRay(pieceRigid.transform.position, directionalForce,Color.red,10.0f);


				Vector2 forceToAdd = directionalForce + randomDir * randomForceOnBreak;
				pieceRigid.AddForce(forceToAdd);
			}
			Destroy(brokenVersion);
			Destroy(gameObject);
		}
	}
}

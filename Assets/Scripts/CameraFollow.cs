﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	public Transform target;
	[Range (0.0f,0.5f)]
	public float xBound = 0.3f;
	[Range (0.0f,0.5f)]
	public float yBound = 0.3f;

	// Use this for initialization
	void Start () {
	
	}

	void OnDrawGizmos() {
		Gizmos.color = new Color(1.0f,0.0f,0.0f,0.5f);

		Vector3 size = Camera.main.ViewportToWorldPoint(new Vector3(xBound*2.0f,yBound*2.0f)) - Camera.main.ViewportToWorldPoint(new Vector3(0.0f,0.0f));
		Gizmos.DrawCube(transform.position, size);
	}

	// Update is called once per frame after the player has moved and everything
	void LateUpdate () {
		Vector2 targetViewportPos = Camera.main.WorldToViewportPoint(target.transform.position);
		Vector2 centerOfScreen = new Vector2(0.5f,0.5f);


		// Get the X position the camera should have
		float xMove = transform.position.x;

		if (targetViewportPos.x > (centerOfScreen.x + xBound)) {
			xMove = Camera.main.ViewportToWorldPoint(new Vector3(targetViewportPos.x - xBound, 0.0f, 0.0f)).x;
		} 

		else if (targetViewportPos.x < (centerOfScreen.x - xBound)) {
			xMove = Camera.main.ViewportToWorldPoint(new Vector3(targetViewportPos.x + xBound, 0.0f, 0.0f)).x;
		}

		// Get the Y position the camera should have
		float yMove = transform.position.y;
		
		if (targetViewportPos.y > (centerOfScreen.y + yBound)) {
			yMove = Camera.main.ViewportToWorldPoint(new Vector3(0.0f, targetViewportPos.y - yBound, 0.0f)).y;
		} 
		
		else if (targetViewportPos.y < (centerOfScreen.y - yBound)) {
			yMove = Camera.main.ViewportToWorldPoint(new Vector3(0.0f, targetViewportPos.y + yBound, 0.0f)).y;
		}

		// Set the new camera position, keep the current Z position
		transform.position = new Vector3(xMove, yMove, transform.position.z);

	}
}

﻿using UnityEngine;
using System.Collections;

public class Killbox : MonoBehaviour {

	void OnTriggerEnter2D (Collider2D other) {
		// Check for player
		if (other.gameObject.CompareTag("Player")) {
			other.GetComponent<MyCharacterController>().Respawn();
		} 
		// Just destroy it if it isnt the player
		else {
			Destroy (other.gameObject);
		}
	}
}

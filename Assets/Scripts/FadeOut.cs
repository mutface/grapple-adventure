﻿using UnityEngine;
using System.Collections;

public class FadeOut : MonoBehaviour {
	public float timeStillToFade = 0.5f;
	private float timeStill;
	public float fadeRate = 4.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		// Check if it has stopped moving
		if (GetComponent<Rigidbody2D>().velocity.magnitude==0.0f) {
			timeStill += Time.deltaTime;
		} else {
			timeStill = 0.0f;
		}

		if (timeStill>=timeStillToFade) {
			// Get current color
			Color spriteColor = GetComponent<SpriteRenderer>().color;

			// Destroy this if it's invisible
			if (spriteColor.a <= 0.0f) Destroy(gameObject);

			// Fade out the alpha
			spriteColor.a -= fadeRate * Time.deltaTime;

			// Set the color
			GetComponent<SpriteRenderer>().color = spriteColor;
		}
	}
}

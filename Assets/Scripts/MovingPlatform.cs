﻿using UnityEngine;
using System.Collections;

public class MovingPlatform : MonoBehaviour {

	public float speed;
	[HideInInspector]
	public Vector2 startingPosition;
	public Vector2 targetPosition;
	private Rigidbody2D rigid;

	private Vector2 moveToThisPos;

	public float secondsToWaitAtTarget = 0.5f;

	void Start () {
		// Get rigidbody for future use
		rigid = GetComponent<Rigidbody2D>();

		// Make sure the start position is set (Editor script also sets this in edit mode)
		startingPosition = rigid.position;

		// Give it a place to move to at the beginning
		moveToThisPos = targetPosition;
	}

	void OnDrawGizmos() {
		// Get color to use from sprite
		Color colorToUse = GetComponent<SpriteRenderer>().color;
		// Cut the alpha in half so it looks ghosty
		colorToUse.a = 0.2f;
		// Set the color
		Gizmos.color = colorToUse;
		
		// Draw start position
		Gizmos.DrawCube((Vector3)startingPosition, GetComponent<Collider2D>().bounds.size);
		// Draw end position
		Gizmos.DrawCube((Vector3)targetPosition, GetComponent<Collider2D>().bounds.size);
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		// Switch positions to move towards when destination is reached
		if (rigid.position == startingPosition) StartCoroutine(DelayTargetChange(secondsToWaitAtTarget,targetPosition));
		if (rigid.position == targetPosition) StartCoroutine(DelayTargetChange(secondsToWaitAtTarget,startingPosition));

		// Move the platform
		Vector2 moveTo = Vector2.MoveTowards(rigid.position, moveToThisPos, speed);
		Vector2 deltaPos = (moveTo-rigid.position);
		rigid.MovePosition(moveTo);

		// Move any passengers
		MovePassengers(deltaPos);
	}

	void MovePassengers(Vector2 moveAmount) {
		// Start from top right corner
		Vector2 origin = GetComponent<Collider2D>().bounds.max + (Vector3) moveAmount;
		// Put ray slightly above platforms
		origin.y += Mathf.Abs(moveAmount.y) + 0.1f;
		// Shoot entire width of platform
		float dist = GetComponent<Collider2D>().bounds.size.x;


		// Find all the objects on top of the platform
		RaycastHit2D[] hits = Physics2D.RaycastAll(origin, -Vector2.right, dist);
		Debug.DrawRay(origin,-Vector2.right*dist,Color.red);

		// Move the player along with the horizontal motion of the platform
		foreach (RaycastHit2D hit in hits) {
			if (hit.transform.CompareTag("Player")) {
				Vector2 playerMoveAmnt = moveAmount;
				playerMoveAmnt.y=0.0f;
				hit.transform.Translate(playerMoveAmnt);
			}
		}
	}

	IEnumerator DelayTargetChange(float secondsToWait, Vector2 newTarget) {
		yield return new WaitForSeconds(secondsToWait);
		moveToThisPos = newTarget;
	}

}
	
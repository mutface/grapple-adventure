﻿using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(MovingPlatform))]
public class MovingPlatformEditor : Editor {

	private MovingPlatform platform;

	void OnEnable() {
		platform = (MovingPlatform) target;

	}


	public override void OnInspectorGUI() {
		// Show default inspector property editor
		DrawDefaultInspector ();

		// Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
		serializedObject.Update ();

		// Make the buttons next to eachother
		GUILayout.BeginHorizontal();
		if (GUILayout.Button ("Zero X")) {
			platform.targetPosition.x = platform.startingPosition.x;
			EditorUtility.SetDirty(target);
		}
		if (GUILayout.Button ("Zero Y")) {
			platform.targetPosition.y = platform.startingPosition.y;
			EditorUtility.SetDirty(target);
		}
		GUILayout.EndHorizontal();

		// Apply changes to the serializedProperty - always do this in the end of OnInspectorGUI.
		serializedObject.ApplyModifiedProperties ();
	}

	void OnSceneGUI () {
		Handles.color = Color.magenta;

		// Draw a handle for moving the target position
		platform.targetPosition =  Handles.PositionHandle ((Vector3)platform.targetPosition, Quaternion.identity);

		// Update the starting position if the game is in edit mode
		if (!Application.isPlaying) platform.startingPosition = platform.transform.position;

		// Make a dotted line between the start and target
		Handles.DrawDottedLine(platform.startingPosition, (Vector3)platform.targetPosition, 10.0f);

		// Make sure to save changes
		if (GUI.changed) EditorUtility.SetDirty(target);

	}

}
